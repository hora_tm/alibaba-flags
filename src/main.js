import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { aliases, fa } from "vuetify/iconsets/fa";
import { mdi } from "vuetify/iconsets/mdi";
import "vuetify/styles";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";

const lightTheme = {
  dark: false,
  colors: {
    background: "hsl(0, 0%, 98%)",
    text: "hsl(200, 15%, 8%)",
    primary: "hsl(0, 0%, 100%)",
    input: "hsl(0, 0%, 52%)",
  },
};
const darkTheme = {
  dark: true,
  colors: {
    background: "hsl(207, 26%, 17%)", //very dark blue
    primary: "hsl(209, 23%, 22%)", //for elements
    text: "hsl(209, 23%, 22%)",
    input: "hsl(0, 0%, 52%)",
  },
};
const vuetify = createVuetify({
  icons: {
    defaultSet: "mdi",
    aliases,
    sets: {
      mdi,
    },
  },
  theme: {
    defaultTheme: "lightTheme",
    themes: {
      lightTheme,
      darkTheme,
    },
  },
  components,
  directives,
});

createApp(App).use(store).use(vuetify).use(router).mount("#app");
