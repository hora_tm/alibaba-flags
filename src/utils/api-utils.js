import api from "@/services/api";

export const getAllCountries = async () => {
  try {
    let res = await api.get("all");
    return res.data;
  } catch (err) {
    throw new Error(err.response.data.message || "Error Happend");
  }
};

export const getCountryById = async (id) => {
  console.log("country", id);
  try {
    let res = await api.get("alpha/" + id);
    console.log(res);
    return res.data[0];
  } catch (err) {
    throw new Error(err.response.data.message || "Error Happend");
  }
};

export const searchCountryByName = async (name) => {
  console.log("country", name);
  try {
    let res = await api.get("name/" + name);
    console.log(res);
    return res.data;
  } catch (err) {
    throw new Error(err.response.data.message || "Error Happend");
  }
};

export const getCountryByRegion = async (region) => {
  console.log("region", region);
  try {
    let res = await api.get("region/" + region);
    console.log(res);
    return res.data;
  } catch (err) {
    throw new Error(err.response.data.message || "Error Happend");
  }
};
