import axios from "axios";

const instance = axios.create({
  baseURL: "https://restcountries.com/v3.1/",
  headers: {
    "Content-Type": "application/json",
  },
});

// instance.interceptors.request.use(async (config) => {
//   return config;
// });

// const api = instance;
export default instance;
